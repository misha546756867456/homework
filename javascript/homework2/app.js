let number = +prompt(`Enter your number`);

if (number < 5 && number > 0 || number > -5 && number < 0) {
    console.log(`Sorry, no numbers`);
}
else if (number > 0) {
    let i;
    for (i = 0;  i < number; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
}
else if (number < 0) {
    for (let i = 0;  i < -number; i++) {
        if (i % 5 === 0) {
            console.log(-i);
        }
    }
}
