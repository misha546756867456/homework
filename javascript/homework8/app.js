const input = document.getElementById('text-input');
const newSpan = document.createElement('span');
const newButton = document.createElement('button');
const doc = document.querySelector('.label');
newButton.textContent = 'X';
let textNode = document.createTextNode('Please enter correct price ');

input.addEventListener('focus',  (evt) => {
    evt.style.cssText = 'border: 3px solid green';
});

input.addEventListener('blur',  (evt) => {
    let event = evt.target;
    event.style.cssText = 'border: none';
    event.style.backgroundColor = 'green';
    if (event.value <= 0) {
        event.style.backgroundColor = 'red';
        doc.prepend( textNode );
    } if (event.value >= 1) {
        newSpan.innerHTML = `Текущая цена: ${event.value}`;
        doc.before( newSpan );
        doc.append( newButton );
        event.value = '';
        textNode.textContent = '';
    }
});

newButton.addEventListener("click", function () {
    newButton.remove();
    newSpan.innerHTML = '';
    input.value = '';
});
