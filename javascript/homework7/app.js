const createElem = function (my_list) {
    const newList = document.createElement('ul');
    const doc = document.body;

    let newMapList = my_list.map((element) => {
    let newEl = document.createElement('li');
        newEl.textContent = element;
        return newEl;
    });

    newMapList.forEach(el => {
        newList.append(el);
        doc.append(newList);
    });

    doc.prepend(newList);
};

createElem(['1','2', '3', 'user', ' sea ', 23]);
