$(document).ready(function() {
    $('a[href^="#"]').click(function() {
        let target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 1000);
    })
});
let topBtn = document.getElementById('topBtn');
$(window).scroll(function(){
    if($(window).scrollTop() > 800){
        $(topBtn).show()
    }
});
$('.hideBtn').click(function () {
    $(".section-top-rated").slideToggle("slow");
});