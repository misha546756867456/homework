const showPass         = document.getElementById('show-pass');
const dontShowPass     = document.getElementById('dont-show-pass');
const openPass         = document.getElementById('input');
const openPass2        = document.getElementById('input2');
const myButton         = document.getElementById('button');
const labelWrapper     = document.getElementById('second-label');
let errorValue         = document.createElement("p");
errorValue.textContent = 'Нужно ввести одинаковые значения';

showPass.onclick = function (event) {
    let input = document.getElementById('input');
    window.firstValue = input.value;

    event.target["classList"].toggle('fa-eye-slash');
    openPass.removeAttribute('type');
    if (event.target["classList"].contains('fa-eye-slash')) {
        input.type = 'text';
    } else {
        input.type = 'password';
    }
};

dontShowPass.onclick = function (event) {
    let input2 = document.getElementById('input2');
    window.secondValue = input2.value;

    event.target["classList"].toggle('fa-eye');
    openPass2.removeAttribute('type');
    if (event.target["classList"].contains('fa-eye')) {
        input2.type = 'text';
    } else {
        input2.type = 'password';
    }
};

myButton.onclick = function () {
    if (firstValue === secondValue) {
        errorValue.textContent = '';
        alert('You are welcome');
    } else {
        labelWrapper.after(errorValue);
        errorValue.style.color = "red";
    }
};
