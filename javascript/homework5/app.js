let userName = prompt('Enter your name: ');
let userSurname = prompt('Enter your surname: ');
let userDate =  prompt('Enter your date(dd.mm.yyyy): ');
let d = new Date;

const createNewUser = function (firstName, lastName, userDate) {
    return {
        userName: firstName,
        userSurname: lastName,
        getLogin: function () {
            return (`${this.userName[0]}${this.userSurname}`);
        },
        birthday: this.userDate,
        getAge: function () {
            let userAge = d.getFullYear() - userDate.slice(-4);
            return (`${userAge} years old`);
        },
        getPassword: function () {
            return (`${this.userName[0].toUpperCase()}${this.userSurname}${userDate.slice(-4)}`);
        },
    }
};

const u = createNewUser(userName.toUpperCase(), userSurname.toLowerCase(), userDate);

console.log(u.getAge());
console.log(u.getLogin());
console.log(u.getPassword());