let images = document.querySelectorAll('.image-to-show');
let currentImg = 0;
let slider = setInterval(imgSlider, 5000);
function imgSlider() {
    images[currentImg].classList.remove('current-img');
    currentImg = (currentImg + 1) % images.length;
    images[currentImg].classList.add('current-img');
}
const btnContainer = document.createElement('div');
btnContainer.classList.add('btn-wrapper');
document.querySelector("script").before(btnContainer);

const stopTimeOut = document.createElement('button');
stopTimeOut.classList.add('btn');
stopTimeOut.innerText = 'Прекратить показ';
btnContainer.appendChild(stopTimeOut);
stopTimeOut.addEventListener('click', () => {
    clearInterval(slider);
});
const resumeTimeOut = document.createElement('button');
resumeTimeOut.classList.add('btn');
resumeTimeOut.innerText = 'Возобновить показ';
btnContainer.appendChild(resumeTimeOut);
document.querySelector('script').before(btnContainer);
resumeTimeOut.addEventListener('click', () => {
    slider = setInterval(imgSlider, 5000)
});











