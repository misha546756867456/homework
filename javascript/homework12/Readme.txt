Опишите своими словами разницу между функциями setTimeout() и setInterval().
Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

1) С помощью setTimeout() мы можем вызывать функцию один раз через определённый интервал времени.
А с помощью setInterval() мы можем вызывать функцию регулярно, через определённый интервал времени.

2)Нет, функция стработает не мгновенно, можно взять такой пример
где программа выполнит сначала скрипт а потом отложеный вызов который мы прописали в setTimeout()

setTimeout(() => console.log('World'), 0);
console.log('Hello');


3) Для того чтобы clearInterval() прервал досрочное выполнение отложеной функции