const filterBy = function (myList, elementType) {
   return myList.filter(item => typeof item !== elementType);
};

console.log(filterBy(['hello', 'world', 23, '23', null], `string`));