const swiper1 = document.querySelector('.swiper-container');
const swiper2 = document.querySelector('.slider-bottom');

let swiperTop = new Swiper(swiper1, {
    slidesPerView: 1,
    loopedSlides: 4,

    direction: 'horizontal',
    loop: true,
    slideToClickedSlide: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

let swiperBottom = new Swiper(swiper2, {
    slidesPerView: 4,
    direction: 'horizontal',
    loop: true,
});

swiperTop.controller.control = swiperBottom;
swiperBottom.controller.control = swiperTop;
