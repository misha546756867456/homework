// Our Services Tabs
const tabsEl = document.querySelectorAll('.tabs-title');
const textEl = document.querySelectorAll('.tab');

let tabName;
tabsEl.forEach(el => {
    function selectTabEl() {
        tabsEl.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-description');
        selectTabText(tabName);
    }
    function selectTabText(tabName) {
        textEl.forEach(item => {
            if (item.classList.contains(tabName)) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }
        })
    }
    el.addEventListener('click', selectTabEl);
});

//Our Amazing Work Tabs
function amazingTabs() {
    const amazingWorkTab = document.querySelectorAll('.work-tabs');
    let hiddenIm         = document.querySelectorAll('.hiddenImg');
    let hiddenContent    = document.querySelector('.hiddenImage');
    let btnHidden        = document.getElementById('onclickBtn');
    
    hiddenContent.style.display = 'none';
    btnHidden.onclick = function() {
        hiddenContent.style.cssText = 'display: grid; ' +
            'grid-template-columns: repeat(4, 291px);';
    };
    let amazingTabName;
    amazingWorkTab.forEach(aTab => {
       function selectAmazingTab() {
           amazingTabName = this.getAttribute('data-work-description');
           selectImg(amazingTabName);
       }
       function selectImg(amazingTabName) {
           hiddenIm.forEach(item => {
               if (item.classList.contains(amazingTabName)) {
                   item.removeAttribute( 'hidden');
               } else {
                   item.setAttribute('hidden', 'true');
               }
           });
       }
       aTab.addEventListener('click', selectAmazingTab);
    });
}
amazingTabs();

