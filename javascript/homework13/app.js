let darkMode      = localStorage.getItem("darkMode");
let currentLink   = document.getElementById('currentLink');
const doc         = document.body.classList;
const darkModeBtn = document.querySelector(".change-theme");

if (darkMode === "enabled") {
    doc.add("darkMode");
    currentLink.setAttribute('href', 'style.css');
} else {
    currentLink.setAttribute('href', 'style2.css');
}

darkModeBtn.addEventListener("click", () => {
    darkMode = localStorage.getItem("darkMode");
    if (darkMode !== "enabled") {
        doc.add("darkMode");
        localStorage.setItem("darkMode", "enabled");
        currentLink.setAttribute('href', 'style.css');
    } else {
        localStorage.setItem("darkMode", null);
        currentLink.setAttribute('href', 'style2.css');
    }
});