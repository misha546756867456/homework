const headerBurger = document.querySelector('.header__burger');
const headerMenu = document.querySelector('.dropdown-menu');
const dropdownLink = document.querySelector('.dropdown__link');
headerBurger.addEventListener('click', () => {
    headerBurger.classList.toggle('active');
    headerMenu.classList.toggle('active');
    dropdownLink.classList.toggle('active');
});
