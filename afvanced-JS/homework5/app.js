const ipFoundBtn  = document.querySelector('.ip-find');
const ipUrl       = 'https://api.ipify.org/?format=json';
const list        = document.createElement('ul');

ipFoundBtn.addEventListener('click', () => {
    async function ipFoundFn(ipUrl) {
        const url = await fetch(ipUrl);
        return await url.json();
    }
    ipFoundFn(ipUrl)
        .then(ip => console.log(createInfoPersonDOMList(ip)))
        .catch(error => console.log(error));
});

function createInfoPersonDOMList(info) {
    const listEl = document.createElement('li');
    listEl.innerHTML = JSON.stringify(info);
    list.appendChild(listEl);
    document.body.append(list);
}

async function getPersonInfo(info) {
    const getInfo = await fetch(`http://ip-api.com/json/?fields=${info}`);
    return await getInfo.json();
}
getPersonInfo(['continent,country,region,regionName,city'])
    .then(r => console.log(createInfoPersonDOMList(r)))
    .catch(error => console.log(error));
