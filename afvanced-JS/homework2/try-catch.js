const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const myList = document.createElement('ul');
const parentEl = document.getElementById('root');

const newMapList = books.map((element) =>
{
    let listEl = document.createElement('li');
    listEl.textContent = JSON.stringify(element);
    function createElement() {
        myList.append(listEl);
        parentEl.append(myList);
        return listEl;
    }
    try {
        for (let elementKey in element) {
            if(element.hasOwnProperty('price') &&
                element.hasOwnProperty('name') &&
                element.hasOwnProperty('author'))
            {
                createElement();
            } else {
                listEl.remove();
            }
        }
    } catch (error) {
        console.log(error);
        throw new Error('Incorrect object');
    }
});
