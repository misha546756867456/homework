1) Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

Допустим у нас есть два класса, и нам нужно чтобы наследуемый класс имел досутп к полям из родительского

Например:
    class Car {
        constructor(speed, color) {
            this.speed = speed;
            this.color = color;
        }
        stop() {
            return this.speed = 0;
        }
    }

    class Reno extends Car {
        constructor(...args) {
            super(...args);
        }
        carSpeed() {
            return `reno speed ${this.speed}`;
        }
    }

    const reno = new Reno (
        120, 'red'
    );