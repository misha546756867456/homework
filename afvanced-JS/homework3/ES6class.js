class Employer {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
    set salary(salary) {
        return this._salary * 6;
    }

    get age() {
        return this._age;
    }
    set age(salary) {
        return this._age;
    }

    get name() {
        return this._name;
    }
    set name(salary) {
        return this._name;
    }
}

class Programmer extends Employer {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get lang() {
        return this._lang;
    }
    set lang(lang) {
        return this._lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(_salary) {
        return this._salary;
    }
}

const firstEmployer = new Employer('John', 35, 45000, ['Java', 'Python', 'Ruby', 'Scala']);

const firstProgrammer = new Programmer('Steve', 24, 10000, ['JavaScript', 'Go', 'PHP', 'Docker', 'Rust']);
const secondProgrammer = new Programmer('Walter', 48, 23000, ['.NET', 'Ruby', 'C++', 'C#', 'Swift']);
console.log(firstProgrammer);
console.log(secondProgrammer);

