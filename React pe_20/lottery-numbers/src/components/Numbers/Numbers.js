import React, { Component } from 'react'
import Button from '../Button/Button';

class Numbers extends Component {
	
	render() {
		let { randomNumbers } = this.props;
		console.log(randomNumbers);

		return (
			<div>
				{randomNumbers.map((e, index) => {
					return <p>{e}<Button/></p>
				})}
			</div>
		)
	}
}

export default Numbers; 