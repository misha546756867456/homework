import React, { Component } from 'react'

class Button extends Component {

	render() {
	let {deleteElementHandler} = this.props
		return (
			<button onClick={deleteElementHandler()}>X</button>
		)
	}
}

export default Button; 