import React, { Component } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Numbers from "./components/Numbers/Numbers";

class App extends Component {
  state = {
    randomNumbers: [],
  };

  randomNumberHandler() {
    let { randomNumbers } = this.state;
    let r = Math.floor(Math.random() * 36);
    this.setState({randomNumbers: [...randomNumbers, r]})
  }

  deleteElementHandler() {
    let { randomNumbers } = this.state;
    this.setState({randomNumber: randomNumbers = ''})
  }

  render() {
    console.log(this.state.randomNumbers);
    return (
      <div className="App">
        <button onClick={() => this.randomNumberHandler()}>Generate</button>
        <Button deleteElementHandler={this.deleteElementHandler()} />
        <Numbers randomNumbers={this.state.randomNumbers}/>
      </div>
    );
  }
}

export default App;
