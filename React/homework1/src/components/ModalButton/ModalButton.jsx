import React, {Component} from "react";

class ModalButton extends Component {
   render() {
      const {
         firstModalBtnText,
         firstModalSecondBtnText,
         secondModalFirstText,
         secondModalSecondBtnText,
         modalClose,
         modalCloseSecond
      } = this.props;
      return (
          <div className="header-login__btn-wrapper">
             <button onClick={modalClose || modalCloseSecond} type="submit"
                     className="header-login__btn">
                {firstModalBtnText}
                {secondModalFirstText}
             </button>
             <button onClick={modalClose || modalCloseSecond} type="submit"
                     className="header-login__btn">
                {firstModalSecondBtnText}
                {secondModalSecondBtnText}
             </button>
          </div>
      );
   }
}

export default ModalButton;
