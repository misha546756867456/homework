import React, { Component } from "react";

class ClosingCross extends Component {
  render() {
    const { handleModalClose } = this.props;

    return (
      <div>
        <div onClick={handleModalClose} className="close__modal-wrapper">
          <p className="close__first-modal-stick close"></p>
          <p className="close__second-modal-stick close"></p>
        </div>
      </div>
    );
  }
}

export default ClosingCross;
