import React, { Component } from "react";

class Button extends Component {
  render() {
    const { backgroundColor, onClickHandler, text } = this.props;

    return (
      <div> 
        <button
          style={{ backgroundColor: backgroundColor }}
          onClick={onClickHandler}
          onClick={onClickHandler}
          className="button first-page__button"
        >
          {text}
        </button>
      </div>
    );
  }
}

export default Button;
