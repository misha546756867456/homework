import React, {Component} from "react";
import ClosingCross from "../CrossForClosing/ClosingCross";

class Modal extends Component {

   render() {
      const {text, header, closeButton, action, modalClose} = this.props;
      const cross = <ClosingCross handleModalClose={modalClose}/>;
      return (
          <>
             <div className="first-modal-wrapper">
                <div className="modal-bg-wrapper"
                     onClick={modalClose}/>
                <div className="modal-inner">
                   <div className="modal login">
                      <p className="modal__header-text">
                         {header}
                         {closeButton && cross}
                      </p>
                      <p className="modal__main-text">
                         {text}
                         {action}
                      </p>
                   </div>
                </div>
             </div>
          </>
      );
   }
}

export default Modal;