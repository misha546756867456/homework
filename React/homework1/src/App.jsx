import React, {Component} from "react";
import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";
import ModalButton from "./components/ModalButton/ModalButton";
import "./App.scss";

class App extends Component {
   state = {
      isModalOpen: false,
      isSecondModalOpen: false,
      darkBd: true,
   };

   handleModalClose() {
      this.setState({isModalOpen: !this.state.isModalOpen});
   }

   handleModalCloseSecond() {
      this.setState({isSecondModalOpen: !this.state.isSecondModalOpen});
   }

   render() {
      const {isModalOpen, isSecondModalOpen} = this.state;
      return (
          <div className="App">
             {isModalOpen && (
                 <>
                    <Modal
                        modalClose={() => this.handleModalClose()}
                        text={"This top-notch code was not written in one day!"}
                        header={"Want to download music?"}
                        closeButton={true}
                        action={
                           <ModalButton modalClose={() => this.handleModalClose()}
                                        secondModalFirstText={"Ok"}
                                        secondModalSecondBtnText={"Cancel"}
                           />
                        }
                    />
                 </>
             )}
             <div className="second-modal-wrapper modals">
                {isSecondModalOpen && (
                    <>
                       <Modal
                           modalClose={() => this.handleModalCloseSecond()}
                           text={"Are u sure that yo" +
                           "u want to delete second modal ?\n Consequences can be disastering!!!"}
                           header={"Do you want to delete this file?"}
                           closeButton={true}
                           action={
                              <ModalButton modalClose={() => this.handleModalCloseSecond()}
                                           secondModalFirstText={"Ok"}
                                           secondModalSecondBtnText={"Cancel"}
                              />
                           }
                       />
                    </>
                )}
             </div>
             <div className="button__wrapper">
                <Button
                    backgroundColor={"green"}
                    text={"Open First Modal"}
                    onClickHandler={() => this.handleModalClose()}
                />
                <Button
                    backgroundColor={"purple"}
                    text={"Open Second Modal"}
                    onClickHandler={() => this.handleModalCloseSecond()}
                />
             </div>
          </div>
      );
   }
}

export default App;
