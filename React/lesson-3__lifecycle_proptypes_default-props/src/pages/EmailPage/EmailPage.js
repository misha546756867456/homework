import React, { Component } from "react";
import Email from "../../components/Email/Email";
import EmailsList from "../../components/EmailsList/EmailsList";

class EmailPages extends Component {
  state = {
    email: {},
  };

  componentDidMount() {
    fetch("emails.json")
      .then((r) => r.json())
      .then((allEmails) => {
        const currentEmail = allEmails.find(
          (email) => email.id === this.props.match.params.emailID
        );
        this.setState({ email: currentEmail });
      });
  }

  render() {
	  const {email} = this.state;
    return(email && <Email self={email} />);
  }
}
export default EmailPages;
