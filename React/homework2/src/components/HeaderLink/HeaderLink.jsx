import React, { Component } from "react";
import PropTypes from 'prop-types'

class HeaderLink extends Component {
  render() {
    const { text } = this.props;
    return (
      <a className="header__list-link" href="#">
        {text}
      </a>
    );
  }
}

export default HeaderLink;

HeaderLink.propTypes = {
  text: PropTypes.string
};
