import React, { Component } from "react";
import HeaderList from "./HeaderList/HeaderList";
import "./Header.scss";

class Header extends Component {
  state = {
    links: [],
  };

  componentDidMount() {
    this.setState({ links: ["home", "portfolio", "contacts", "gallery"] });
  }

  render() {
    const { links } = this.state;
    return (
      <header className="header container">
        <div className="img-wrapper">
          <img
            src="https://1.bp.blogspot.com/-lKEigWDX38Y/XXiRiTch0ZI/AAAAAAAAAJw/EIv0hL9-dY05Xza-0VT9xWgB9PiyAdRugCLcBGAsYHQ/s1600/logo%2B129.png"
            alt="logo"
          />
        </div>

        <HeaderList items={links} />
      </header>
    );
  }
}

export default Header;
