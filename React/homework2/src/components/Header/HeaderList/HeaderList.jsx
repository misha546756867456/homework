import React, { Component } from "react";
import HeaderLink from "../../HeaderLink/HeaderLink";
import PropTypes from 'prop-types';

class HeaderList extends Component {
  render() {
    const { items } = this.props;
    const itemsList = items.map((item, index) => {
      return (
        <li className="header__list-item" key={index}>
          <HeaderLink text={item} />
        </li>
      );
    });
    return <ul className="header__list">{itemsList}</ul>;
  }
}

export default HeaderList;

HeaderList.propTypes = {
  items: PropTypes.array
};