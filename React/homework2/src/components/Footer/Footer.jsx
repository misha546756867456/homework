import React, {Component} from 'react';
import "./Footer.scss"

class Footer extends Component {
   render() {
      return (
          <footer>
             <div className="container">
                <div className="little-about-us">
                   <h1 className='little-about-us__title'>
                      Little about us
                   </h1>
                   <p className='little-about-us__text'>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque autem, expedita magni maxime
                      molestiae nihil non numquam porro qui, quis, quod tempore totam!
                   </p>
                </div>
                <div className="our-contact">
                   <h2 className="our-contact-title"><span>Our contact: </span>099-111-22-33</h2>
                </div>
             </div>
          </footer>
      );
   }
}

export default Footer;