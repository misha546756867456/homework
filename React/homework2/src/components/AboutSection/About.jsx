import React, {Component} from "react";
import "./About.scss";
import CardsList from "../CardSection/CardList";

class About extends Component {
   state = {
      listOfSong: [],
   };

   componentDidMount() {
      fetch('db.json')
          .then(data => data.json())
          .then(card => {
             this.setState({listOfSong: card})
          })
   }

   render() {
      const {listOfSong} = this.state;
      console.log(listOfSong);

      return (
          <div>
             <div className="about-section">
                <p className="about-section-text">
                   Music Event With Dj Starting at 20.00 on august 15th
                </p>
             </div>
             <p className="about-section-subtitle">
                welcome to <span className="red-text">music,</span> check our latest albums
             </p>
             <CardsList collection={listOfSong}/>
          </div>
      );
   }
}

export default About;
