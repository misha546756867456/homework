import React, {Component} from 'react';
import PropTypes from 'prop-types'

class Button extends Component {
    render() {
        const { onClickHandler, text } = this.props;
        return (
            <button onClick={onClickHandler}
                    className='card-add-to-btn'>{text}</button>
        );
    }
}

export default Button;

Button.propTypes = {
    onClickHandler: PropTypes.func,
    text: PropTypes.string
};