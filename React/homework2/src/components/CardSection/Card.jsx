import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import './Card.scss'

class Card extends Component {
   state = {
      showCross: false,
      wishes: [],
   };

   render() {
      const {imgUrl, name, price, color, addToCart, addToFavorites} = this.props;
      return (
          <div className="singleCard">
             <img src={imgUrl}/>
             <div className="stars"
                  data-stars="1">
                <svg onClick={addToFavorites}
                     height="25"
                     width="23"
                     className="star rating"
                     data-rating="1">
                   <polygon
                       points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78"
                   />
                </svg>
             </div>
             <p className="card-name">{name}</p>
             <p className="card-price">{price}</p>
             <Button text="add to cart"
                     onClickHandler={addToCart}/>
             <p>color: {color}</p>
          </div>
      );
   }
}

export default Card;

Card.propTypes = {
   imgUrl: PropTypes.string,
   name: PropTypes.string,
   price: PropTypes.string,
   color: PropTypes.string,
};