import React, {Component} from 'react';
import Card from "./Card";
import PropTypes from 'prop-types'
import Modal from "../Modal/Modal";

class CardsList extends Component {
   state = {
      open: false,
      currentId: null,
      cart: [],
      favs: [],
   };

   addToCart() {
      const {cart, currentId} = this.state;
      this.setState({cart: [...cart, currentId]}, () => {
         localStorage.setItem('cart', JSON.stringify(this.state.cart))
      });
   }

   addToFavorites() {
      const {favs, currentId} = this.state;
      this.setState({favs: [...favs, currentId]}, () => {
         localStorage.setItem('favs', JSON.stringify(this.state.favs))
      })
   }

   modalState(forceValue) {
      this.setState({open: forceValue ? forceValue : !this.state.open})
   }

   setCurrentId(id) {
      this.setState({currentId: id})
   }

   modalOpen() {
      this.setState({open: !this.state.open});
   }

   render() {
      const {collection} = this.props;
      const {open, showCross} = this.state;

      const cardsList = collection.map(card => {
         return (
             <Card
                 key={card.id}
                 className="card"
                 imgUrl={card.imgUrl}
                 name={card.name}
                 price={card.price}
                 color={card.color}
                 id={card.id}
                 addToCart={() => {
                    this.setCurrentId(card.id);
                    this.modalOpen();
                 }}
                 addToFavorites={() => {
                    this.setCurrentId(card.id);
                    this.addToFavorites();
                 }}
             />
         )
      });

      return (
          <>
             <div className="cards-wrapper container">
                {collection.length > 0 && cardsList}
             </div>
             {open && (
                 <>
                    <Modal
                        toggleModal={() => this.modalState(false)}
                        actions={
                           <>
                              <button onClick={() => {
                                 this.addToCart();
                                 this.modalState(false);
                              }}
                                      className="close__modal-wrapper">
                                 Yes
                              </button>

                              <button
                                  onClick={() => this.modalState(false)}
                                  className="close__modal-wrapper">
                                 No
                              </button>
                           </>
                        }
                        closeButton={showCross}
                    />
                 </>
             )}
          </>
      );
   }
}

export default CardsList;

CardsList.propTypes = {
   collection: PropTypes.array,
};
