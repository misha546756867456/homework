import React, {Component} from "react";
import Header from './components/Header/Header'
import About from './components/AboutSection/About';
import Footer from "./components/Footer/Footer";
import './App.css';

class App extends Component {

    render() {
        return (
            <div className="App">
                <Header/>
                <About/>
                <Footer />
            </div>
        );
    }
}

export default App;
