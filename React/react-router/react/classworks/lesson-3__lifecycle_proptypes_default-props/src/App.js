import './App.scss';
import './Emails.scss'
import Head from "./components/Head/Head";
import React, {Component} from "react";
import LoginForm from "./components/LoginForm/LoginForm";

class App extends Component {
  state = {
    currentPage: 'home',
  };

  handlePageChange(nextPage) {
    this.setState({currentPage: nextPage})
  }

  getCurrentPageComponent() {
    const {currentPage} = this.state;
    switch (currentPage) {
      case "home":
        return <h1>HOME PAGE</h1>;
      case "login":
        return <LoginForm pageChanger={(nP) => this.handlePageChange(nP)}/>;
      case 'welcome':
        return <h1>Welcome, my little programmer))</h1>;
      case 'error':
        return <h1>Error occurred...probably bad credentials</h1>
    }
  }

  render() {
    const {currentPage} = this.state;
    const currentPageComponent = this.getCurrentPageComponent();

    return (
      <>
        <Head openLogin={(nP) => this.handlePageChange(nP)}/>

        {currentPageComponent}

        {/*<div className="container emails">*/}
        {/*  <aside className="emails-sidebar">*/}
        {/*    <ul className="emails-sidebar__list">*/}
        {/*      <li className="emails-sidebar__item emails-sidebar__item--active">Inbox</li>*/}
        {/*      <li className="emails-sidebar__item">Sent</li>*/}
        {/*      <li className="emails-sidebar__item">Draft</li>*/}
        {/*      <li className="emails-sidebar__item">Spam</li>*/}
        {/*    </ul>*/}
        {/*  </aside>*/}
        {/*  */}
        {/*  <section className="emails-list">*/}
        {/*    <div className="emails-list__item">*/}
        {/*      <a href="mailto:gogio@go.com" className="emails-list__link-from">gogio@go.com</a>*/}
        {/*      <h4 className="emails-list__subject">This is it</h4>*/}
        {/*      <p className="emails-list__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aperiam autem dignissimos dolore dolorum ducimus et eveniet exercitationem, hic impedit ipsa iure laborum minima minus non odit quo repudiandae sit!</p>*/}
        {/*    </div>*/}
        {/*  </section>*/}
        {/*</div>*/}
      </>
    );
  }
}

export default App;
