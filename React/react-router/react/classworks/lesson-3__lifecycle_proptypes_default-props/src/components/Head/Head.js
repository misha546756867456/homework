import React, {Component} from 'react';
import NavList from "./NavList/NavList";
import NavButton from "../NavButton/NavButton";

class Head extends Component {
  state = {
    links: [],
  };

  componentDidMount() {
    this.setState({links: ['home', 'portfolio', 'contacts', 'gallery']})
  }

  render() {
    const {links, isModalOpen} = this.state;
    const {openLogin} = this.props;

    return (
      <header className="container header-wrapper">
        <img src="https://picsum.photos/200/300" alt="logo" className='header__company-logo'/>

        <NavList items={links}/>

        <NavButton handleClick={() => openLogin('login')}/>
      </header>
    );
  }
}

export default Head;