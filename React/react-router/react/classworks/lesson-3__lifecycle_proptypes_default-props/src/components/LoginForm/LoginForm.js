import React, {Component, createRef} from 'react';

const credentials = {
  login: 'admin',
  password: '123',
};

class LoginForm extends Component {
  loginRef = createRef();
  passwordRef = createRef();

  handleSubmit(event) {
    event.preventDefault();
    const {pageChanger} = this.props;

    if (this.loginRef.value === credentials.login &&
      this.passwordRef.value === credentials.password) {
      pageChanger('welcome');
    } else {
      pageChanger('error');
    }
  }

  mapInputLogin(loginInput) {
    this.loginRef = loginInput;
    console.log(loginInput);
  }

  mapPasswordInput(password) {
    this.passwordRef = password;
    console.log(password);
  }

  render() {
    return (
      <form onSubmit={(event) => this.handleSubmit(event)} className="login" action="">
        <input ref={loginInput => this.mapInputLogin(loginInput)} type="text" className="login__input"
               placeholder="Enter login"/>
        <input ref={password => this.mapPasswordInput(password)} type="password" className="login__input"
               placeholder="Enter password"/>
        <button type="submit" className="header__login-btn">Login</button>
      </form>
    );
  }
}

export default LoginForm;