export const selectProducts = (currentStore) => currentStore.products;
export const selectFavouritesID = (currentStore) => currentStore.favourite;