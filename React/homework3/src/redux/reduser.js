import {CARD_PAGE_INFO, FAVS_PAGE_INFO, MODAL_OPEN_INFO, FORM_INFO} from './actionTypes'

const initialStore = {
	user: null,
	prods: [],
	favs: [],
	testProp: 'here we are in redux'
};

const reducer = (currentStore = initialStore, action) => {
	switch (action.type) {
		case CARD_PAGE_INFO:
			return {
				...currentStore,
				cards: action.payload,
			};
		case FAVS_PAGE_INFO:
			return {
				...currentStore,
				favorites: action.payload,
			};
		case MODAL_OPEN_INFO:
			return {
				...currentStore,
				isModalOpen: action.payload,
			};
		case FORM_INFO:
			return {
				...currentStore,
				formInfo: action.payload
			};
		default:
			return currentStore
	}
};

export default reducer;