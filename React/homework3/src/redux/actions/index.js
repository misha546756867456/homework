import {FAVS_PAGE_INFO, CARD_PAGE_INFO} from "../actionTypes";

export const saveFavsChosenAction = (favsChosen) => ({
	type: FAVS_PAGE_INFO,
	payload: favsChosen,
});

export const saveListOfChosenAction = (favsChosen) => ({
	type: CARD_PAGE_INFO,
	payload: favsChosen,
});