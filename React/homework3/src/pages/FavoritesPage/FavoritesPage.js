import React, {useEffect, useState} from 'react';
import './FavoritesPage.scss'
import Card from "../../components/CardSection/Card";
import {connect} from "react-redux";
import {saveFavsChosenAction} from "../../redux/actions";

const FavoritesPage = ({favsList}) => {
	const [favsChosen, setFavsChosen] = useState([]);

	const storageCart = JSON.parse(localStorage.getItem('favs'));
	const getLocalTodo = () => {
		fetch('db.json')
			.then((data => data.json()))
			.then(res => {
				setFavsChosen(
					storageCart.map((item) => res.find(jsonItem => jsonItem.id === item))
				);
			})
	};

	useEffect(() => {
		getLocalTodo();
	}, []);

	const deleteFavsFromServer = (id) => {
		localStorage.removeItem('favs');
		const index = id;
		setFavsChosen([...favsChosen.filter((e) => e.id !== index)]);
		localStorage.setItem('favs', JSON.stringify([...favsChosen.filter((e) => e.id === index)]));
	};

	favsList(favsChosen);

	let listOfFavs = favsChosen.map(el => {
		let star = <svg onClick={() => {
			deleteFavsFromServer(el.id);
		}}
		                height="25"
		                width="23"
		                className="star__active btn"
		                data-rating="1" >
			<polygon
				points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78"
			/>
		</svg >;
		return (
			<Card
				key={el.id}
				id={el.id}
				star={star}
				className="card"
				imgUrl={el.imgUrl}
				name={el.name}
				price={el.price}
				color={el.color}
			/>)
	});


	return (
		<div >
			<h1 className='favorite-page' >Favorites Section</h1 >
			<div className="list-wrapper container" >
				{listOfFavs}
			</div >
		</div >
	);
};

const mapDispatchToProps = (dispatch) => ({
	favsList: (favsChosen) => dispatch(saveFavsChosenAction(favsChosen))
});

export default connect(() => ({}), mapDispatchToProps)(FavoritesPage);