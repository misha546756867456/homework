import React, {useEffect, useState} from "react";
import Card from "../../components/CardSection/Card";
import ClosingCross from "../../components/CrossForClosing/ClosingCross";
import Modal from "../../components/Modal/Modal";
import {connect} from "react-redux";
import "./CardPage.scss"
import {saveListOfChosenAction} from "../../redux/actions";
import CardPageForm from "../../components/Form/Form";

const CardPage = ({songsList}) => {
	let [listOfSong, setListOfSong] = useState([]);
	const [showForm, setShowForm] = useState(false);
	const [open, setOpen] = useState(false);
	const [currentID, setCurrentID] = useState(null);

	const storageCart = JSON.parse(localStorage.getItem('cart'));
	const getLocalTodo = () => {
		fetch('db.json')
			.then((data => data.json()))
			.then(res => {
				setListOfSong(
					storageCart.map((item) => res.find(jsonItem => jsonItem.id === item)),
				);
			})
	};

	useEffect(() => {
		getLocalTodo();
	}, []);

	const handleModalClose = (id) => {
		setOpen(!open);
		setCurrentID(id);
	};

	const handleShowForm = () => {
		setShowForm(!showForm);
	};

	songsList(listOfSong);

	const list = listOfSong.map(el => {
		const cross = <ClosingCross handleModal={handleModalClose.bind(this, el.id)} />;
		return (<Card
			className="card"
			key={el.id}
			imgUrl={el.imgUrl}
			name={el.name}
			price={el.price}
			color={el.color}
			id={el.id}
			cross={cross}
		/>)
	});

	const deleteCartFromCard = () => {
		localStorage.removeItem('cart');
		setOpen(false);
		const index = currentID;
		setListOfSong([...listOfSong.filter((e) => e.id !== index)]);
		localStorage.setItem('cart', JSON.stringify([...listOfSong.filter((e) => e.id === index)]));
	};

	return (
		<>
			{showForm && <CardPageForm toggleModal={handleShowForm} />}
			<div className="container" >
				<div className="card-page-title-wrapper" >
					<h1 className='card-page-title' >Card Page</h1 >
					<a
						href="#"
						className="content-bottom-info__link pay-your-order"
						onClick={handleShowForm} >
						<button >Оформить заказ</button >
					</a >
				</div >
				<div className="list-wrapper" >
					{list}
				</div >
				{open && (
					<>
						<div className="list-wrapper" >
						</div >
						<Modal
							header={'Are you sure ? This Album so good'}
							toggleModal={handleModalClose}
							actions={
								<>
									<button onClick={() => {
										handleModalClose();
										deleteCartFromCard()
									}}
									        className="close__modal-wrapper" >
										Yes
									</button >
									<button
										className="close__modal-wrapper" >
										No
									</button >
								</>
							}
						/>
					</>
				)}
			</div >
		</>
	);
};

const mapDispatchToProps = (dispatch) => ({
	songsList: (listOfSong) => dispatch(saveListOfChosenAction(listOfSong))
});

export default connect(() => ({}), mapDispatchToProps)(CardPage);