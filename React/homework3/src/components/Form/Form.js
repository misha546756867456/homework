import React from 'react';
import {Field, Formik, Form, ErrorMessage} from 'formik'
import './Form.scss'
import loginForm from "./validationSchema";
import {useDispatch} from "react-redux";
import {FORM_INFO} from "../../redux/actionTypes";

const CardPageForm = ({toggleModal}) => {
	const dispatch = useDispatch();

	const handleSubmit = values => {
		console.log(values);
		dispatch({type: FORM_INFO, payload: values});
		console.log(localStorage.getItem('cart'));
		console.group(values);
		localStorage.removeItem('cart');
		toggleModal()
	};

	return (
		<div className="form-wrapper" >
			<div className="form-bg-wrapper"
			     onClick={toggleModal} />
			<div className="form-inner" >
				<Formik
					onSubmit={handleSubmit}
					initialValues={{
						name: '',
						surname: '',
						age: '',
						address: '',
						phone: ''
					}}
					validationSchema={loginForm}
				>
					{formProps => {
						return <Form noValidate
						             className='form' >
							<Field
								className='form-item'
								name='name'
								placeholder='Name:'
								component='input'
							/>
							<ErrorMessage className="error-message"
							              component='span'
							              name='name' />
							<Field
								className='form-item'
								name='surname'
								placeholder='Surname:'
								component='input'
							/>
							<ErrorMessage className="error-message"
							              component='span'
							              name='surname' />
							<Field
								className='form-item'
								name='age'
								placeholder='Age:'
								component='input'
							/>
							<ErrorMessage className="error-message"
							              component='span'
							              name='age' />
							<Field
								className='form-item'
								name='address'
								placeholder='Address:'
								component='input'
							/>
							<ErrorMessage className="error-message"
							              component='span'
							              name='address' />
							<Field
								className='form-item'
								name='phone'
								placeholder='Phone number:'
								component='input'
							/>
							<ErrorMessage className="error-message"
							              component='span'
							              name='phone' />
							<button className='checkout-button'
							        type='submit'
							        disabled={formProps.isSubmitting} >
								Checkout
							</button >
						</Form >
					}}
				</Formik >
			</div >
		</div >
	);
};

export default CardPageForm;
