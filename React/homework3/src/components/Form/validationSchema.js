import * as yup from 'yup';

const loginForm = yup.object().shape({
	name: yup.string()
		.min(4, 'Too Short Name!')
		.max(50, 'Too Long Name!')
		.matches(/[abcdefghijklmnopqrstuvwxyz!@#$%^&*(){}]+/ , 'Is not in correct format')
		.required('This field is required'),
	surname: yup.string()
		.min(5, 'Too Short Surname!')
		.max(50, 'Too Long Surname!')
		.matches(/[abcdefghijklmnopqrstuvwxyz!@#$%^&*(){}]+/ , 'Is not in correct format')
		.required('This field is required'),
	age: yup.number()
		.required('This field is required'),
	address: yup.string()
		.matches(/[abcdefghijklmnopqrstuvwxyz!@#$%^&*(){}]+/ , 'Is not in correct format')
		.required('This field is required'),
	phone: yup.number()
		.min(10, 'Min length is 10')
		.required('This field is required')
});

export default loginForm;