import React from 'react';
import PropTypes from 'prop-types'

const Button = ({onClickHandler, text}) => {
   return (
       <button onClick={onClickHandler}
               className='card-add-to-btn'>{text}</button>
   );
};

export default Button;

Button.propTypes = {
   onClickHandler: PropTypes.func,
   text: PropTypes.string
};