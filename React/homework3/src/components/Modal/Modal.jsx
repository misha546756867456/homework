import React from "react";
import "./Modals.scss"
import PropTypes from "prop-types"

const Modal = ({actions, toggleModal, header}) => {
	return (
		<>
			<div className="modal-wrapper" >
				<div className="modal-bg-wrapper"
				     onClick={toggleModal} />
				<div className="modal-inner" >
					<div className="modal login" >
						<p className="modal__header-text" >
							{header}
						</p >
						<p className="modal__main-text" >
							{actions}
						</p >
					</div >
				</div >
			</div >
		</>
	);
};

export default Modal;

Modal.propTypes = {
	btn: PropTypes.object
};
