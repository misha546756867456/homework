import React from "react";
import HeaderLink from "../../HeaderLink/HeaderLink";
import PropTypes from 'prop-types';

const HeaderList = ({items}) => {
   const itemsList = items.map((item, index) => {
      return (
          <li className="header__list-item"
              key={index}>
             <HeaderLink text={item}/>
          </li>
      );
   });
   return <ul className="header__list">{itemsList}</ul>;
};

export default HeaderList;

HeaderList.propTypes = {
   items: PropTypes.array
};