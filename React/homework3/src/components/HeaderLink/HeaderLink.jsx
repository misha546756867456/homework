import React from "react";
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";

const HeaderLink = ({text}) => {
	return (
		<Link
			className="header__list-link"
			to={`/${text.toLowerCase()}`} >
			{text}
		</Link >
	);
};

export default HeaderLink;

HeaderLink.propTypes = {
	text: PropTypes.string
};
