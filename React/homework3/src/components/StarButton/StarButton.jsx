import React, {useState} from 'react';

const StarButton = ({addToFavorites}) => {
	const [activeClassName, setActiveClassName] = useState('');
	const active = false;

	const setActiveStar = () => {
		active ? setActiveClassName("star__active btn") : setActiveClassName("star__active btn");
	};
	return (
		<>
			<svg onClick={() => {
				addToFavorites();
				setActiveStar();
			}}
			     height="25"
			     width="23"
			     className={activeClassName}
			     data-rating="1" >
				<polygon
					points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78"
				/>
			</svg >
		</>
	);
};

export default StarButton;
