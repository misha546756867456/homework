import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import {Switch, Route} from 'react-router-dom';
import './Card.scss'
import StarButton from '../StarButton/StarButton';
import '../../pages/FavoritesPage/FavoritesPage.scss'

const Card = ({
	              imgUrl,
	              name,
	              price,
	              color,
	              addToCart,
	              addToFavorites,
	              cross,
	              star,
              }) => {

	return (
		<div className="singleCard" >
			<img src={imgUrl}
			     alt="" />
			{cross}
			<div className="stars"
			     data-stars="1" >
				<Switch >
					<Route exact
					       path='/favorites' >
						{star}
					</Route >
					<Route exact
					       path='/' >
						<StarButton addToFavorites={addToFavorites} />
					</Route >
				</Switch >
			</div >
			<p className="card-name" >{name}</p >
			<p className="card-price" >{price}</p >
			<Button text="add to cart"
			        onClickHandler={addToCart} />
			<p className='card-color' >color: {color}</p >
		</div >
	)
		;
};

export default Card;

Card.propTypes = {
	imgUrl: PropTypes.string,
	name: PropTypes.string,
	price: PropTypes.string,
	color: PropTypes.string,
};

