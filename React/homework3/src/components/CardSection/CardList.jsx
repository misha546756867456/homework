import React, {useState} from "react";
import PropTypes from "prop-types"
import Modal from "../Modal/Modal";
import Card from "./Card";
import {connect} from "react-redux";

const CardsList = ({collection, dispatch}) => {
	const [cart, setCart] = useState([]);
	const [favs, setFavs] = useState([]);
	const [open, setOpen] = useState(false);
	const [currentID, setCurrentID] = useState(null);

	const addToCart = () => {
		setCart([...cart, currentID]);
		localStorage.setItem('cart', JSON.stringify([...cart, currentID]))
	};

	const addToFavorites = (currentID) => {
		setFavs([...favs, currentID]);
		localStorage.setItem('favs', JSON.stringify([...favs, currentID]))
	};

	const modalOpen = () => {
		setOpen(!open);
		dispatch({type: 'MODAL_OPEN_INFO', payload: open});
	};

	const setCurrentId = (id) => {
		setCurrentID(id)
	};

	const cardsList = collection.map(card => {
		return (
			<Card
				key={card.id}
				className="card"
				imgUrl={card.imgUrl}
				name={card.name}
				price={card.price}
				color={card.color}
				id={card.id}
				starActive={false}
				addToCart={() => {
					setCurrentId(card.id);
					modalOpen();
				}}
				addToFavorites={() => {
					addToFavorites(card.id);
				}}
			/>
		)
	});


	return (
		<>
			<div className="cards-wrapper container" >
				{collection.length > 0 && cardsList}
			</div >
			{open && (
				<>
					<Modal
						header={'Add album to card'}
						toggleModal={() => {
							modalOpen()
						}}
						actions={
							<>
								<button onClick={() => {
									addToCart();
									modalOpen()
								}}
								        className="close__modal-wrapper" >
									Yes
								</button >
								<button
									className="close__modal-wrapper" >
									No
								</button >
							</>
						}
					/>
				</>
			)}
		</>
	);
};

const mapStateToProps = (reduxState) => {
	return {
		modalState: reduxState.modalState,
	}
};

export default connect(mapStateToProps)(CardsList);

CardsList.propTypes = {
	collection: PropTypes.array,
};
