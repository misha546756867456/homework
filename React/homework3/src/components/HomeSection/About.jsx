import React, {useState, useEffect} from "react";
import CardsList from "../CardSection/CardList";
import "./About.scss";

const About = () => {
	const [listOfSong, setListOfSong] = useState([]);

	const getLocalTodo = () => {
		fetch('db.json')
			.then(data => data.json())
			.then(card => {
				setListOfSong(card);
			})
	};

	useEffect(() => {
		getLocalTodo();
	}, []);

	return (
		<div >
			<div className="about-section" >
				<p className="about-section-text" >
					Music Event With Dj Starting at 20.00 on august 15th
				</p >
			</div >
			<p className="about-section-subtitle" >
				welcome to <span className="red-text" >music,</span > check our latest albums
			</p >
			<CardsList collection={listOfSong} />
		</div >
	);
};

export default About;
