import React from "react";
import "./ClossForClosing.scss"

const ClosingCross = ({handleModal}) => {
	return (
		<div onClick={handleModal}
		     className="cross__modal-wrapper">
			<p className="close__first-modal-stick close"> </p>
			<p className="close__second-modal-stick close"> </p>
		</div >
	);
};

export default ClosingCross;