import React from 'react';
import {Route, Switch} from "react-router-dom";
import CardPage from "../pages/CardPage/CardPage";
import About from "../components/HomeSection/About";
import FavoritesPage from "../pages/FavoritesPage/FavoritesPage";

const AppRoutes = () => {
	return (
		<>
			<Switch >
				<Route exact
				       path="/"
				       component={About} />
				<Route exact
				       path="/home"
				       component={About} />
				<Route exact
				       path="/card"
				       component={CardPage} />
				<Route exact
				       path="/favorites"
				       component={FavoritesPage} />
			</Switch >
		</>
	);
};

export default AppRoutes;